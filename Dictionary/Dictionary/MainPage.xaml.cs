﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Dictionary
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        // clicking the button grabs the definition, type, and example
        async void GetDef_Clicked(object sender, System.EventArgs e)
        {
            HttpClient client = new HttpClient();

            // creates an HTTPCLIENT object
            string s = entry.Text;
            var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + s));

            // this is what is what we build and gets sent to the server
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;

            // this is what the server builds and gives back to us
            // here we would be sent the definition, type, and example of the word sent
            HttpResponseMessage response = await client.SendAsync(request);
            DictionaryData data = null;
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                data = DictionaryData.FromJson(content);
                theDefinition.Text = $"Word is {DictionaryData.Definition[0]}";
                theType.Text = $"Type is {Definition[0].Type}";
                theExample.Text = $"Example: {DefinitionData.Example}";
            }

        }
    }
    }
